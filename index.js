const express = require('express');
const sql = require('sqlite3').verbose();

const db = new sql.Database('db1');
const app = express();
// app.use(require('sanitize').middleware);
// show details of bus
app.get('/bus', (req, res) => {
  db.all('select * from bus;', (err, result) => {
    if (err) res.status(500).send(err);
    else res.send(result);
  });
});
// add bus
app.post('/bus', (req, res) => {
  const { from } = req.params;
  const { to } = req.params;
  const { seats } = req.params;
  console.log(from,to,seats)
  const query = `insert into bus('dep' , 'arrival' , 'seats') values('${from}','${to}','${seats}')`;
  db.run(query, (err) => {
    if (err) {
      res.status(500).send(err);
    }
    return (0);
  });
  db.all('SELECT busid FROM bus ORDER BY busid DESC LIMIT 1', (err, result) => {
    if (err) {
      res.status(500).send(err);
      return (0);
    }
    const id = result[0].busid;
    res.send(JSON.stringify(`${id}`, null, 3));
    return (0);
  });
});
// book ticket
app.post('/busid/:busid', (req, res) => {
  const { busid } = req.params;
  const { name } = req.params;
  const { phone } = req.params;
  console.log(name,phone)
  db.all(`select seats from bus where busid=${busid};`, (err, result1) => {
    if (err) {
      res.status(500).send(err);
      return (0);
    }
    if (result1.length === 0) {
      res.status(500).send('check bus_id');
      return (0);
    }
    const seatno = result1[0].seats;
    console.log(seatno)
    if (seatno <= 0) {
      res.status(500).send('no seats avilable');
    } else {
      db.run(`update bus set seats=${seatno - 1} where busid=${busid}`);
      db.all(`insert into details('busid','seatno','name','phone') values(${busid},${seatno},'${name}',${phone})`, (err1, result3) => {
        db.all('SELECT ticketid FROM details ORDER BY ticketid DESC LIMIT 1', (err2, result4) => {
          if (err2 + err1) {
            console.log(err1,err2)
            res.status(500).send(err2 + err1);
            return (0);
          }
          const ticketid = result4[0];
          res.send(JSON.stringify(`${ticketid}`, null, 3));
          return (0);
        });
      });
    }
    return (0);
  });
});
// update ticket
app.put('/update/ticketid/:ticketid', (req, res) => {
  const q = req.params;
  const { ticketid } = q;
  delete q.ticketid;
  let s = '';
  let i = '';
  for (i in q) {
    s += `${i}='${q[i]}',`;
  }
  s = s.substr(0, s.length - 1);
  const query = `update details set ${s} where ticketid=${ticketid}`;
  db.run(query, (err, result) => {
    if (err) {
      res.status(500).send(err);
      return (0);
    }
    res.send('done');
    return (0);
  });
});
// ticket status
app.get('/status/ticketid/:ticketid', (req, res) => {
  const { ticketid } = req.params;
  console.log(ticketid)
  db.all(`select * from details where ticketid=${ticketid}`, (err, result) => {
    if (err) {
      res.status(500).send(err);
      return (0);
    }
    if (result.length === 0) {
      res.status(400).send('chech ticketid.');
      return (0);
    }
    res.send(JSON.stringify(result[0], null, 3));
    return (0);
  });
});
// openticket
app.get('/openticket', (req, res) => {
  db.all('select * from details where open=1', (err, result) => {
    if (err) {
      res.status(500).send(err);
      return (0);
    }
    res.send(JSON.stringify(result, null, 3));
    return (0);
  });
});
// closeticket
app.get('/closeticket', (req, res) => {
  db.all('select * from details where open=0', (err, result) => {
    if (err) {
      res.status(500).send(err);
      return (0);
    }
    res.send(JSON.stringify(result, null, 3));
    return (0);
  });
});
// close bus
app.put('/closebus/busid/:busid', (req, res) => {
  const { busid } = req.params;
  db.run(`update details set open=0 where busid=${busid}`, (err, result) => {
    if (err) {
      res.status(500).send(err);
      return (0);
    }
    res.send('done');
    return (0);
  });
});
app.listen(process.env.PORT || 5000);
